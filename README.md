### 账号密码可关注公众号“好码农”免费获取，欢迎大家踊跃提建议

>在iOS开发中有时需要做马甲包或上线审核时因为4.3被拒，为了使应用能够通过审核，这时就需要批量修改类名了，一个个的手动去改会比较耗时还容易报错，这个时候就需要一个批量修改类名的工具了，轻轻松松一分钟就搞定所有的类名修改。话不多说，上菜上菜。

一、先获取账号密码

![1.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/376e5b9783cb41dea124c4c0981c9931~tplv-k3u1fbpfcp-watermark.image?)

二、修改类名

目前工具里还只有一个修改类名的功能，后期会加上代码混淆、垃圾代码添加、方法名修改和界面风格修改的功能。

![3.png](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/771e471490514167b524f674fe6f0125~tplv-k3u1fbpfcp-watermark.image?)

**1、项目路径** 

这个项目路径是所有代码所在的路径，就是跟项目名一样的文件夹，例如：

![4.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/a921fc67fdcd455b90d5a4a6afa6672f~tplv-k3u1fbpfcp-watermark.image?)

**2、目标路径**

目标路径就是你需要修改的类名是在哪个文件夹下，你可以把项目路径作为目标路径，但是不建议这样做，因为项目路径是包括了项目所有的类，如果项目的体量比较大的话会比较耗时，建议一个一个文件夹来修改。想要修改的类名在哪个文件夹下就把哪个文件夹作为目标路径。
**3、project.pbxproj文件路径
找到项目文件里的.xcodeproj，右键显示包内容就能找到project.pbxproj文件

![5.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/ee0ee02c711940019bb5ad1496224e32~tplv-k3u1fbpfcp-watermark.image?)

![6.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/7f4dbd2eaa8e49f995ff99a8c6832bc6~tplv-k3u1fbpfcp-watermark.image?)

**4、填上需要替换的文件前缀和新的文件前缀**

目前只能根据前缀进行替换，后期会进行优化。一套下来是不是很简单呢，快上手试试吧。

三、添加垃圾代码

**1、新建文件夹**

新建一个文件夹用来存放垃圾代码。

**2、添加垃圾代码**

将新建的文件夹拖到目标路径。
![2-3.png](https://upload-images.jianshu.io/upload_images/7823606-8242665d6b9276ef.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

**3、填写类名前缀**

类名前缀可根据自己的需求决定是否填写

**4、将文件夹添加到项目中**

点击添加按钮代码生成成功后，将垃圾代码添加成功后到文件夹加入到项目中。

>目前添加垃圾代码的功能还不够完善，下一步将会进行完善，添加以下功能：
1、将增加自定义类名前缀和后缀，目前是写死的；
2、增加完整的单词来命名类名、方法名和属性，目前是随机字母拼凑而成；

**有什么需要完善功能欢迎大家提出，有不好的地方也欢迎大家提出批评。**

 

### 账号密码可关注公众号“好码农”免费获取，欢迎大家踊跃提建议

